import del from 'del'
import * as fs from 'fs'
import gitClone from 'git-clone'

const repo = 'https://github.com/PokeAPI/pokeapi.git'

const csvPath = 'pokeapi/data/v2/csv/'

const filesToRead = [
  'pokemon',
  'types',
  'type_efficacy',
  'pokemon_types'
]
const filesToReadLn = filesToRead.length

interface IDamageKey {
  name: keyof IDamageRelations
  value: number
}

const damageKeys: IDamageKey[] = [
  { name: 'noDamageTo', value: 0 },
  { name: 'halfDamageTo', value: 50 },
  { name: 'normalDamageTo', value: 100 },
  { name: 'doubleDamageTo', value: 200 }
]

interface IPokemon {
  name: string
  types: any
}

interface IDamageRelations {
  noDamageTo: string[]
  halfDamageTo: string[]
  normalDamageTo: string[]
  doubleDamageTo: string[]
  noDamageFrom: string[]
  halfDamageFrom: string[]
  normalDamageFrom: string[]
  doubleDamageFrom: string[]
}

interface ITypes {
  name: string
  damageRelations: IDamageRelations
}

const pokemon: { [key: number]: IPokemon } = {}
const types: { [key: number]: ITypes } = {}

const clone = () => new Promise((resolve, reject) => {
  gitClone(repo, 'pokeapi', (error: any) => {
    if (error) {
      return reject(error)
    }
    return resolve()
  })
})

const execute = async () => {
  await del(['json', 'pokeapi', 'sprites'])

  console.log(`Cloning ${repo}...`)

  await clone()

  console.log(`Finished cloning`)

  if (!fs.existsSync('json')) {
    fs.mkdirSync('json')
  }

  for (let i = 0; i < filesToReadLn; i++) {
    const fileName = filesToRead[i]
    const csvData = fs.readFileSync(`${csvPath}${fileName}.csv`, 'utf8')

    const rows = csvData.split('\n')
    let columns: string[] = []

    rows.forEach((row: string, j: number) => {
      const cells: string[] = row.split(',')
      if (j === 0) {
        columns = cells
        return
      }
      if (cells.length === 1) {
        return
      }
      switch (fileName) {
        case 'pokemon':
          pokemon[parseInt(cells[columns.indexOf('id')], 10)] = {
            name: cells[columns.indexOf('identifier')],
            types: {}
          }
          break
        case 'types':
          types[parseInt(cells[columns.indexOf('id')], 10)] = {
            damageRelations: {
              doubleDamageFrom: [],
              doubleDamageTo: [],
              halfDamageFrom: [],
              halfDamageTo: [],
              noDamageFrom: [],
              noDamageTo: [],
              normalDamageFrom: [],
              normalDamageTo: []
            },
            name: cells[columns.indexOf('identifier')]
          }
          break
        case 'pokemon_types':
          const pokemonID = parseInt(cells[columns.indexOf('pokemon_id')], 10)
          const slot = cells[columns.indexOf('slot')]
          pokemon[pokemonID].types[slot] = cells[columns.indexOf('type_id')]
          break
        default:
          const thisType = parseInt(cells[columns.indexOf('damage_type_id')], 10)
          const opponentType = parseInt(cells[columns.indexOf('target_type_id')], 10)
          const damage = parseInt(cells[columns.indexOf('damage_factor')], 10)
          const damageKey: IDamageKey | undefined = damageKeys.find(d => d.value === damage)
          if (damageKey) {
            types[thisType].damageRelations[damageKey.name].push(types[opponentType].name)
          }
      }
    })
  }

  Object.keys(pokemon).forEach(id => {
    const idNum = parseInt(id, 10)
    const slotKeys = Object.keys(pokemon[idNum].types)
    slotKeys.sort()
    const thisNewTypes: string[] = []
    slotKeys.forEach(k => {
      thisNewTypes.push(types[pokemon[idNum].types[k]].name)
    })
    pokemon[idNum].types = thisNewTypes
  })

  const newTypes: {[key: string]: IDamageRelations} = {}

  Object.keys(types).forEach(id => {
    const type = types[parseInt(id, 10)]
    newTypes[type.name] = type.damageRelations
  })

  Object.keys(newTypes).forEach(type => {
    Object.keys(newTypes[type]).filter(e => /To$/.test(e)).forEach(e => {
      const effect = e as keyof IDamageRelations
      newTypes[type][effect].forEach((type2: string) => {
        const fromEffect = effect.replace('To', 'From') as keyof IDamageRelations
        newTypes[type2][fromEffect].push(type)
      })
    })
  })

  fs.writeFileSync('json/pokemon.json', JSON.stringify(pokemon))
  fs.writeFileSync('json/types.json', JSON.stringify(newTypes))

  console.log('Saved `json/pokemon.json` and `json/types.json`')

  if (!fs.existsSync('sprites')) {
    fs.mkdirSync('sprites')
  }

  const spritesDir = 'pokeapi/data/v2/sprites/pokemon/'

  const allSprites = fs.readdirSync(spritesDir)
  const spritesToCopy = allSprites.filter((s: string) => /^\d+\.png$/.test(s))
  const spritesLn = spritesToCopy.length
  for (let i = 0; i < spritesLn; i++) {
    const fileName = spritesToCopy[i]
    fs.copyFileSync(`${spritesDir}${fileName}`, `sprites/${fileName}`)
  }

  console.log(`${spritesLn} sprites copied`)

  await del(['pokeapi'])
}

execute()
